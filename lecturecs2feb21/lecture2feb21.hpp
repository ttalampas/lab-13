class charstack {
private:
	char *stack;
	int max;
	int size = 0;
public:
	charstack(int newmax);
	~charstack();
	bool push(char c);
	char pop();
	bool isempty() const;
};
